import struct
import time

from python.pygodot.com import GodotUDPCom
from python.pygodot.serialize import GodotSerialize


def test(udp_ip, port=8080):
    com = GodotUDPCom(udp_ip, port)
    errors = []

    def check_data(data, received_data, print_data=True):
        nonlocal errors
        if received_data['status'] != 'ok':
            print('   => ERROR : ' + received_data['data'])
            errors.append(data)
            return

        received_data = received_data['data']
        if isinstance(data, type(received_data)) and data == received_data:
            print('    => OK')
            if print_data:
                print(f'PYTHON : {data=}\n' + f'{received_data=}')
        elif data != data and received_data != received_data:  # nan test
            print('    => OK')
            if print_data:
                print(f'PYTHON : {data=}\n' + f'{received_data=}')
        else:
            print(' => ERROR')
            print(f'   {type(data)=} {type(received_data)=}')
            if print_data:
                print(f'   {data == received_data=}')
                print(f'   Send     : {GodotSerialize.dumps(data)}')
                print(f'   Received : {com._message[48:]}')
            errors.append(data)

    def cast_float(data: float):
        return struct.unpack('<f', struct.pack('<f', data))[0]

    def cast_double(data: float):
        return struct.unpack('<d', struct.pack('<d', data))[0]

    def cast_floats(values: list[float]):
        return tuple([struct.unpack('<f', struct.pack('<f', value))[0] for value in values])

    list_data = []
    test_data = [None]  # null
    list_data.append(test_data[-1])

    test_data += [True, False]  # bool
    list_data.append(test_data[-2])

    test_data += [0, 1, 128, 16777216, 2147483647, -2147483648, -16777216, -128, -1]  # int
    list_data.append(test_data[-7])

    test_data += [cast_float(value) for value in [
        0.0, 0.1, 0.5, -0.0, -0.1, -0.5,
        float(2**127) * (2 - 2**-23), -float(2**127) * (2 - 2**-23), 2**-149, -2**-149,
        float('inf'), float('-inf'), float('nan')]]  # float
    list_data.append(test_data[-8])

    test_data += [cast_double(value) for value in [
        2**-1074, -2**-1074, float(2**1023), -float(2**1023), float('nan')]]  # double
    list_data.append(test_data[-3])

    test_data += ['', 't', '雲', 'te', 'tes', 'test', 'longer test\nfo see\rto']  # str
    list_data.append(test_data[-4])

    test_data += [cast_floats(value) for value in [(0.5, -1), (2, -0.1)]]  # Vector2
    list_data.append(test_data[-1])

    test_data += [cast_floats(value) for value in [(0.5, -1, 2, -0.5)]]  # rect2
    list_data.append(test_data[-1])

    test_data += [cast_floats(value) for value in [(0.5, -1.0, 4e-8)]]  # Vector3
    list_data.append(test_data[-1])

    test_data += [cast_floats(value) for value in [(0.5, -1, 2, -0.5, -1.0, 2e-8)]]  # transform2d
    list_data.append(test_data[-1])

    test_data += [{'test': None, 0.0: 25, 3: 'data'}, {'a': {1: (cast_float(0.5), cast_float(-8))}}]  # dictionary
    list_data.append(test_data[-1])

    test_data += [list_data]  # array

    test_data += [b'test12345', b'\x00\x20\xff\x46']  # raw array

    test_data += [[0, 1, 2], [-2, -128, 65535]]  # int array

    test_data += [list(cast_floats([0.0, -0.1, 89.12]))]  # real array

    test_data += [['test', 'abc', 'this is working!雲']]  # string array

    test_data += [[cast_floats([0.5, -100.9]), cast_floats([-0.5, 100.9])]]  # vector2 array

    test_data += [[cast_floats([0.5, -100.9, 5.2]), cast_floats([-0.5, 100.9, -5.2])]]  # vector3 array

    test_data += [[cast_floats([0.5, -100.9, 5.2, 1.0]), cast_floats([-0.5, 100.9, -5.2, 1.0])]]  # color array

    try:
        for data in test_data:
            print('PYTHON : sending message')
            com.send_message('echo', data)
            received_data = com.receive_message()
            check_data(data, received_data)
        big_data = [1000, 65_440, 65_500, 1_000_000]
        for data_len in big_data:
            print(f'PYTHON : sending big data (len {data_len})')
            data = b' ' * data_len
            com.send_message('echo', data)
            received_data = com.receive_message()
            check_data(data, received_data, print_data=False)
    except KeyboardInterrupt:
        com.finalize()
    except Exception as error:
        com.finalize()
        com.send_message('stop', None)
        raise error
    com.send_message('stop', None)
    com.receive_message()
    print()
    print(f'OK     : {len(test_data) + len(big_data) - len(errors)}/{len(test_data) + len(big_data)}')
    print(f'ERRORS : {len(errors)}/{len(test_data) + len(big_data)}')


def speed_test(udp_ip, port=8080):
    com = GodotUDPCom(udp_ip, port)

    def bench(action, request_count, packet_size):
        data = b' ' * packet_size if packet_size else None
        start_time = time.time()
        if action == 'echo':
            for _ in range(request_count):
                com.send_message(action, data)
                _ = com.receive_message()
        elif action == 'recv':
            for _ in range(request_count):
                com.send_message(action, data)
        elif action == 'send':
            com.send_message(action, {'count': request_count, 'data': data})
            for _ in range(request_count):
                _ = com.receive_message()
        stop_time = time.time()
        if packet_size:
            size_ratio = 512 if action == 'echo' else 1024
            print(f'Sent {request_count} requests of len {packet_size} in {stop_time - start_time:.02f}s'
                  f' => {request_count / (stop_time - start_time):.02f}req/s'
                  f' => {request_count * packet_size / size_ratio / (stop_time - start_time):.02f}kiB/s')
        else:
            print(f'Sent {request_count} requests in {stop_time - start_time:.02f}s'
                  f' => {request_count / (stop_time - start_time):.02f}req/s')

    print('Starting speed benchmark')
    try:
        for action in ['echo', 'recv', 'send']:
            print(f'Benchmark action : {action}')
            for request_count, packet_size in [
                    (5000, 0), (5000, 100), (4000, 1_000), (4000, 10_000),
                    (800, 100_000), (80, 1_000_000), (4, 10_000_000)]:
                bench(action, request_count, packet_size)
    except KeyboardInterrupt:
        com.finalize()
    except Exception as error:
        com.finalize()
        com.send_message('stop', None)
        raise error
    com.send_message('stop', None)
    com.receive_message()
    print('Speed benchmark done')
