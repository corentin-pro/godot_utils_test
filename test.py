from argparse import ArgumentParser
import subprocess
import time
import traceback

from python.test import test, speed_test


def main():
    parser = ArgumentParser()
    parser.add_argument('--ip', help='IP to connect to (if none local Godot is automatically started)')
    parser.add_argument('--port', type=int, default=8080, help='Port for UDP sockets (default: 8080)')
    parser.add_argument('--speed', action='store_true', help='Benchmark IPC speed')
    arguments = parser.parse_args()

    if not arguments.ip:
        godot_process = subprocess.Popen('godot -s test_loop.gd', cwd="godot", shell=True)
        time.sleep(0.5)
    else:
        godot_process = None
    udp_ip = arguments.ip if arguments.ip else '127.0.0.1'

    try:
        if arguments.speed:
            speed_test(udp_ip)
        else:
            test(udp_ip)
    except Exception:
        traceback.print_exc()

    if godot_process is not None:
        try:
            godot_process.wait(1)
        except subprocess.TimeoutExpired:
            pass
        godot_process.terminate()


if __name__ == '__main__':
    main()
