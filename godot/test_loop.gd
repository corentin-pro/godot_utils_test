extends MainLoop
class_name TestLoop

var com: UDPCom
var com_thread: Thread = Thread.new()
var initialized: bool = false
var should_exit: bool = false


func _initialize() -> void:
	com = UDPCom.new()
	if com.connect("com_init", self, "_on_com_init", [], CONNECT_ONESHOT) != OK:
		print('GODOT : Cannot connect to communication signal')
	if com_thread.start(com, "start", null) != OK:
		print('GODOT : Cannot start communication')
	Engine.iterations_per_second = 2
	Engine.target_fps = 1


func _idle(_delta: float) -> bool:
	if initialized:
		var request: ComMessage.Request
		var response: ComMessage.Response
		var _success
		while true:
			request = com.receive(-1)
			if request == null:
				print('GODOT : received error')
			elif request.action == 'echo':
				response = ComMessage.Response.new(ComMessage.OK, request.data)
				_success = com.send_message(response)
			elif request.action == 'recv':
				pass
			elif request.action == 'send':
				var request_count = request.data['count']
				response = ComMessage.Response.new(ComMessage.OK, request.data['data'])
				for _i in range(request_count):
					_success = com.send_message(response)
			else:
				_success = com.send_message(ComMessage.Response.new(ComMessage.OK, null))
				break
		com.stop()
		print('GODOT : Waiting')
		com_thread.wait_to_finish()
		print('GODOT : Exiting')
		return true
	return false

func _on_com_init() -> void:
	initialized = true

